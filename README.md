# dropbox_content_hash

![](logo.png)



Calculate the content hash of a file, as per dropbox.

Dropbox uses a block hash based on SHA-256. See [their page](https://www.dropbox.com/developers/reference/content-hash) for the details.

This code uses the [openssl](https://www.openssl.org/) library for the hash computations, but it should be trivial to replace it with your favourite SHA-256 hash implementation.

This is free and unencumbered software released into the public domain. For the details see [LICENSE](LICENSE).


[Upload to Cloud](https://icons8.com/icon/48264/upload-to-cloud) icon by [Icons8](ttps://icons8.com).
