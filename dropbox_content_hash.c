// Calculate the content hash of a file, as per dropbox:

// step1: Split the file into blocks of 4 MB (4,194,304 or 4 * 1024 * 1024 bytes). The last block (if any) may be smaller than 4 MB.
// step2: Compute the hash of each block using SHA-256.
// step3: Concatenate the hash of all blocks in the binary format to form a single binary string.
// step4: Compute the hash of the concatenated string using SHA-256. Output the resulting hash in hexadecimal format.
//
// There is no block for an empty file of zero length. In this case an empty string would be formed in step 3 above.


#define	HASH_BLOCK_LENGTH_IN_BYTES              (256 / 8)
#define	DROPBOX_HASH_CHUNK_SIZE_IN_BYTES        (4 * 1024 * 1024)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/sha.h>

void calculateHash(FILE * file, unsigned char * hash)
{
  // file read buffer INP1; its length must be a multiple of HASH_BLOCK_LENGTH_IN_BYTES
  unsigned char INP1[HASH_BLOCK_LENGTH_IN_BYTES * 1024];
  unsigned char INP2[HASH_BLOCK_LENGTH_IN_BYTES];
  unsigned char OUT1[HASH_BLOCK_LENGTH_IN_BYTES];
  unsigned char OUT2[HASH_BLOCK_LENGTH_IN_BYTES];
  int numBytes;
  SHA256_CTX C1, C2;

  SHA256_Init(&C1);
  SHA256_Init(&C2);

  long long blockCounter = 0LL;

  while (!(feof(file))) {
    numBytes = fread(INP1, 1, sizeof(INP1), file);
    if (numBytes > 0) {
      blockCounter = blockCounter + sizeof(INP1) / HASH_BLOCK_LENGTH_IN_BYTES;
      SHA256_Update(&C1, INP1, numBytes);

      if (blockCounter ==
        (long long) DROPBOX_HASH_CHUNK_SIZE_IN_BYTES
        /
        (long long) HASH_BLOCK_LENGTH_IN_BYTES
      ) {
        SHA256_Final(OUT1, &C1);
        SHA256_Init(&C1);
        memcpy(INP2, (const void *) OUT1, sizeof(INP2));
        SHA256_Update(&C2, INP2, sizeof(INP2));
        blockCounter = 0LL;
      }
    }
  }

  if (blockCounter != 0LL) {
    SHA256_Final(OUT1, &C1);
    memcpy(INP2, (const void *) OUT1, sizeof(INP2));
    SHA256_Update(&C2, INP2, sizeof(INP2));
  }
  SHA256_Final(OUT2, &C2);

  // the regular sha256 hash of the file
  //for (i = 0; i < HASH_BLOCK_LENGTH_IN_BYTES; i++) { printf("%02x", OUT1[i]); } printf("\n");

  // the dropbox content hash
  //for (i = 0; i < HASH_BLOCK_LENGTH_IN_BYTES; i++) { printf("%02x", OUT2[i]); } printf("\n");

  memcpy(hash, (const void *) OUT2, sizeof(OUT2));
}

int main(int argc, char *argv[])
{
  FILE *file;
  char *fileName;
  int i, j;
  unsigned char hash[HASH_BLOCK_LENGTH_IN_BYTES];

  if (argc == 1) {
    fprintf(stderr, "Usage: %s file ...\n", argv[0]);
    exit(1);
  }

  for (i = 1; i < argc; i++) {

    fileName = argv[i];
    file = fopen(fileName, "rb");
    if (file == NULL) {
      fprintf(stderr, "could not open file %s\n", fileName);
      exit(2);
    }

    calculateHash(file, hash);

    fclose(file);
    for (j = 0; j < HASH_BLOCK_LENGTH_IN_BYTES; j++) {
      printf("%02x", hash[j]);
    }
    printf(" %s\n", fileName);

  }

  return 0;

}
